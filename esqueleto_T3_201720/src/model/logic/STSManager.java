package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import com.google.gson.Gson;

import model.data_structures.IQueue;
import model.data_structures.IStack;
import model.data_structures.LinkedQueue;
import model.data_structures.LinkedStack;
import model.vo.BusUpdateVO;
import model.vo.StopVO;
import api.ISTSManager;

public class STSManager implements ISTSManager
{
	LinkedQueue<BusUpdateVO> colaBusUpdates;

	ArrayList<StopVO> listStops;

	@Override
	public void readBusUpdate(File rtFile) 
	{
		Gson gson = new Gson();
		BufferedReader reader = null; 
		colaBusUpdates = new LinkedQueue<>();

		try
		{
			reader = new BufferedReader(new FileReader(rtFile));
			BusUpdateVO[] update = gson.fromJson(reader, BusUpdateVO[].class);

			for (int i = 0; i < update.length; i++) 
			{
				colaBusUpdates.enqueue(update[i]);
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally 
		{
			try 
			{
				if(reader != null)
				{
					reader.close();
				}
			} 
			catch (IOException e) 
			{
				e.printStackTrace();
			}
		}
		try 
		{

		} 
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	@Override
	public IStack<StopVO> listStops(Integer tripID) 
	{
		ArrayList<BusUpdateVO> busesConTripId = new ArrayList<>();

		IStack<StopVO> stops = new LinkedStack<>(); 

		BusUpdateVO bus = colaBusUpdates.dequeue();
		while(bus != null)
		{
			//System.out.println("El trip id es" + bus.darTripId());

			if(bus.darTripId() == tripID)

			{
				busesConTripId.add(bus);
				System.out.println("entr� a agregar");
			}

			bus = colaBusUpdates.dequeue(); 
		}

		System.out.println(busesConTripId.size());

		System.out.println("busesConTripId " + busesConTripId.size());
		for(int i = 0; i < busesConTripId.size(); i++)
		{
			bus = busesConTripId.get(i);

			double lat1 = Double.parseDouble(bus.darLatitude());
			double lon1 = Double.parseDouble(bus.darLongitude());

			System.out.println("lon" + bus.darLongitude());

			for(int j = 0; j < listStops.size(); j++)
			{
				StopVO stop = listStops.get(j);

				double distancia = getDistance(lat1, lon1, stop.getStopLat(), stop.getStopLon()); 

				if( distancia <= 70) 
				{
					System.out.println(stop.getName());
				}
			}	
		}

		return stops;
	}
	
	//OJO----Se implement� el c�digo para leer archivos Json igual al presentado en el laboratorio, sin embargo, creemos 
	//que no est� leyendo todos los archivos. 

	public double getDistance(double lat1, double lon1, double lat2, double lon2) 
	{
		// TODO Auto-generated method stub
		final int R = 6371*1000; // Radious of the earth

		Double latDistance = toRad(lat2-lat1);
		Double lonDistance = toRad(lon2-lon1);
		Double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2) + 
				Math.cos(toRad(lat1)) * Math.cos(toRad(lat2)) * 
				Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
		Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
		Double distance = R * c;

		return distance;
	}

	private Double toRad(Double value) 
	{
		return value * Math.PI / 180;
	}

	@Override
	public void loadStops() 
	{
		BufferedReader lector = null;
		listStops = new ArrayList<>();

		try
		{
			lector = new BufferedReader(new FileReader("./data/estatico/stops.txt"));
			String linea = lector.readLine();
			linea = lector.readLine();
			while(linea!= null)
			{
				String[] partes = linea.split(",");

				int stop_id = Integer.parseInt(partes[0]);
				String stop_code = partes[1];
				String stop_name = partes[2];
				String stop_desc = partes[3]; 
				double stop_lat = Double.parseDouble(partes[4]);
				double stop_lon = Double.parseDouble(partes[5]); 
				String zone_id = partes[6];
				String stop_url = partes[7];
				String location_type = partes [8];
				int parent_station = Integer.parseInt(partes[8]);

				StopVO stop = new StopVO(stop_id, stop_code, stop_name, stop_desc, stop_lat, stop_lon, zone_id, stop_url, location_type, parent_station);

				listStops.add(stop);
				linea = lector.readLine();
			}
		}
		catch (Exception e) {

			e.printStackTrace();
		}
		finally 
		{
			if(lector != null)
			{
				try 
				{
					lector.close();
				} 
				catch (IOException e) 
				{
					e.printStackTrace();
				}
			}
		}
	}
}
