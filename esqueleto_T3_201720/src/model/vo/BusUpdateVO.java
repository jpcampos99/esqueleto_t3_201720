package model.vo;

public class BusUpdateVO 
{
	private int VehicleNo; 
	private int TripId;
	private String RouteNo; 
	private String Direction;
	private String Destination;
	private String Pattern;
	private String Latitude; 
	private String Longitude;
	private String RecordedTime;	
	private RouteMap Routemap;
	
	
	public class RouteMap
	{
		String Href;
	}
	
	public int darVehicleNo() 
	{
		return VehicleNo;
	}
	public void setVehicleNo(int vehicleNo) 
	{
		VehicleNo = vehicleNo;
	}
	public int darTripId() 
	{
		return TripId;
	}
	public void setTripId(int tripId) 
	{
		TripId = tripId;
	}
	public String darRouteNo() 
	{
		return RouteNo;
	}
	public void setRouteNo(String routeNo) 
	{
		RouteNo = routeNo;
	}
	public String darDirection() 
	{
		return Direction;
	}
	public void setDirection(String direction) 
	{
		Direction = direction;
	}
	public String darDestination() 
	{
		return Destination;
	}
	public void setDestination(String destination) 
	{
		Destination = destination;
	}
	public String darPattern() 
	{
		return Pattern;
	}
	public void setPattern(String pattern) 
	{
		Pattern = pattern;
	}
	public String darLatitude() 
	{
		return Latitude;
	}
	public void setLatitude(String latitude) 
	{
		Latitude = latitude;
	}
	public String darLongitude() 
	{
		return Longitude;
	}
	public void setLongitude(String longitude) 
	{
		Longitude = longitude;
	}
	public String darRecordedTime() 
	{
		return RecordedTime;
	}
	public void setRecordedTime(String recordedTime) 
	{
		RecordedTime = recordedTime;
	}
}
