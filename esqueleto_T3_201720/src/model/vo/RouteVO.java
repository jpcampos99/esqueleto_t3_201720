package model.vo;

import model.data_structures.Nodo;

public class RouteVO 
{
	private int routeId;
	private String agencyId;
	private String routeShortName;
	private String routeLongName;
	private String routeDesc; 
	private int routeType;
	private String routeUrl;
	private String routeColor; 	
	private String routeTextColor;
	private int min;

	public RouteVO(int pRouteId, String pAgencyId, String pRouteShortName,String pRouteLongName, String pRouteDesc, int pRouteType, String pRouteUrl, String pRouteColor, String pRouteTextColor)
	{
		routeId = pRouteId;
		setAgencyId(pAgencyId); 
		setRouteShortName(pRouteShortName);
		routeLongName = pRouteLongName;
		routeDesc = pRouteDesc;
		routeType = pRouteType;
		routeUrl = pRouteUrl;
		routeColor = pRouteColor;
		routeTextColor = pRouteColor;
	}

	/*
	public VOTrip buscarTrip(int pTripId)
	{
		
		
		Nodo<VOTrip> nodoActual = listaTrips.cabeza();
		int i = 0;
		while(nodoActual != null && i < listaTrips.getSize())
		{
			if(nodoActual.darElemento().getTripId() == pTripId)
			{
				return nodoActual.darElemento();
			}
			nodoActual = nodoActual.darSiguiente();
			i++;
			
		}
		
		
		
		return null;
	}
	*/
	
	//creo que esto no sirve para linked list
	
	/*
	public VOTrip buscarBinarioTrip (int pTripId){

		int inicio = 0;
		int fin = listaTrips.getSize( ) -1;

		while(inicio<= fin )
		{
			int medio = (inicio + fin)/2; 
			if(listaTrips.getElement(medio).getTripId() == pTripId)
			{

				return listaTrips.getElement(medio);
			}   
			else if(listaTrips.getElement( medio ).getTripId() > pTripId )
			{
				fin = medio-1;
			}
			else
			{
				inicio = medio + 1; 
			}


		}
		return null;
	}
	public void addARing(Nodo<VOTrip> n)
	{
		for (int i = 0; i < listaTrips.getSize(); i++) {

			if (listaTrips.getElement(i).getTripId() < n.darElemento().getTripId()) continue;

			listaTrips.addAtk(n, i);
			return;
		}

		listaTrips.addAtEnd(n);
	} 	


	public boolean tieneTrip(IList<String> listaAbuscar)
	{

		for (String voTripBuscar : listaAbuscar) {


			if(buscarBinarioTrip(Integer.parseInt(voTripBuscar)) != null)
			{
				return true;
			}
			
			
		}


		return false;
	}

	public IList<VOTrip> getListaTrips()
	{
		return listaTrips; 
	}


	/**
	 * @return id - Route's id number
	 */
	public int darId() {

		return routeId;
	}

	/**
	 * @return name - route name
	 */
	public String getName() {
	
		return routeLongName;
	}

	public String getAgencyId() {
		return agencyId;
	}

	public void setAgencyId(String agencyId) {
		this.agencyId = agencyId;
	}

	public String getRouteShortName() {
		return routeShortName;
	}

	public void setRouteShortName(String routeShortName) {
		this.routeShortName = routeShortName;
	}


	
	
	
	
	
	
	
}
