package model.vo;

public class StopVO {

	
	private int stopId;
	private String stopCode;
	private String stopName; 	
	private String stopDesc;
	private double stopLat;
	private double stopLon;
	private String zoneId;
	private String stopUrl;
	private String locationType;
	private int parentStation;
	
	
	public StopVO(int pStopId, String pStopCode, String pStopName, String pStopDesc, double pStopLat, double pStopLon, String pZoneId, String pStopUrl, String pLocationType, int pPartenStation)
	
	{
		
		stopId = pStopId;
		stopCode = pStopCode;
		stopName = pStopName;
		stopDesc = pStopDesc;
		setStopLat(pStopLat);
		setStopLon(pStopLon); 
		setZoneId(pZoneId); 
		locationType = pLocationType; 
		parentStation = pPartenStation;
		
	}
	
	
	/**
	 * @return id - stop's id
	 */
	public int id() {
		// TODO Auto-generated method stub
		return stopId;
	}

	/**
	 * @return name - stop name
	 */
	public String getName() {
		// TODO Auto-generated method stub
		return stopName;
	}


	public String getZoneId() {
		return zoneId;
	}


	public void setZoneId(String zoneId) {
		this.zoneId = zoneId;
	}


	public double getStopLat() {
		return stopLat;
	}


	public void setStopLat(double stopLat) {
		this.stopLat = stopLat;
	}


	public double getStopLon() {
		return stopLon;
	}


	public void setStopLon(double stopLon) {
		this.stopLon = stopLon;
	}

	
	
	
	
}
