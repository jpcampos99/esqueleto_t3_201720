package model.data_structures;

public interface IStack<E> 
{
	public class LinkedStack<E> implements IStack<E> 
	{
		Nodo<E> tope;
		
		int size;

		public LinkedStack() 
		{
			tope = null;
			size = 0;
		}
		
		public boolean esVacio() 
		{
			return (size == 0);
		}

		public int size() 
		{
			return size;
		}

		public void push(E elem) 
		{
			Nodo<E> nodo1 = new Nodo<E>( elem, null, null); 
			
			if (tope == null)
			{
				tope = nodo1;
			}
			else 
			{
				nodo1.cambiarSiguiente(tope);
				tope = nodo1;
			}
			size++;
		}; 

		public void pop( ) 
		{
			if ( !esVacio() ) 
			{
	            tope = tope.darSiguiente(); 
	            size--;
	        }
		}

		public E top() throws Exception
		{
	        if( !esVacio() )
	        {
	            return (E) tope.darElemento(); 
	        }
	        else 
	        {
	            throw new Exception("La pila se encuentra vacia.");
	        }
	    }
	} 
}
