package model.data_structures;

public class LinkedStack<T> implements IStack<T> {

	 
	Nodo<T> tope;
	
	int size;

	public LinkedStack() 
	{
		tope = null;
		size = 0;
	}
	
	public boolean esVacio() 
	{
		return (size == 0);
	}

	public int size() 
	{
		return size;
	}

	public void push(T elem) 
	{
		Nodo<T> nodo1 = new Nodo<T>( elem, null, null); 
		
		if (tope == null)
		{
			tope = nodo1;
		}
		else 
		{
			nodo1.cambiarSiguiente(tope);
			tope = nodo1;
		}
		size++;
	}; 

	public T pop( ) 
	{
		
		T elementoPopiado = null;
		
		if ( !esVacio() ) 
		{
			elementoPopiado = tope.darElemento();
            tope = tope.darSiguiente(); 
            
            size--;
        }
		
		return elementoPopiado; 
	}

	public T top() throws Exception
	{
        if( !esVacio() )
        {
            return (T) tope.darElemento(); 
        }
        else 
        {
            throw new Exception("La pila se encuentra vacia.");
        }
    }
} 


