package model.data_structures;

public interface IQueue<E> 
{
	public class LinkedQueue<E> implements IQueue<E>
	{
		private int size;     

		private Nodo<E> cabeza;   

		private Nodo<E> ultimo;    

		public LinkedQueue() 
		{
			cabeza = null;
			ultimo  = null;
			size = 0;
		}

		public boolean esVacio() 
		{
			return (size == 0);
		}

		public int size() 
		{
			return size;
		}

		public void enqueue(E elem) 
		{
			Nodo<E> nodo1 = new Nodo<E>(elem, null, null); 

			if ( esVacio())
			{
				cabeza = nodo1;
			}
			else
			{
				ultimo.cambiarSiguiente(nodo1);
			}

			ultimo = nodo1;
			size++;
		}

		public E dequeue() throws Exception
		{
			if ( esVacio() )
			{
				throw new Exception("No hay elementos en la cola");
			}
			else
			{
				E nodo1 = (E) cabeza.darElemento(); 
				cabeza = cabeza.darSiguiente(); 
				size--;

				if ( esVacio() )
				{
					ultimo = null;
				}
				return nodo1; 
			}
		}

		public E first() throws Exception
		{
			if ( esVacio() )
			{
				throw new Exception("No hay elementos en la cola");
			}
			else
			{
				return (E) cabeza.darElemento(); 
			}
		}    
	}
}
