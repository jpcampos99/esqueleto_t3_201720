package model.data_structures;

public class LinkedQueue<T> implements IQueue<T>
{
	private int size;     

	private Nodo<T> cabeza;   

	private Nodo<T> ultimo;    

	public LinkedQueue() 
	{
		cabeza = null;
		ultimo  = null;
		size = 0;
	}

	public boolean esVacio() 
	{
		return (size == 0);
	}

	public int size() 
	{
		return size;
	}

	public void enqueue(T elem) 
	{
		Nodo<T> nodo1 = new Nodo<T>(elem, null, null); 

		if ( esVacio())
		{
			cabeza = nodo1;
		}
		else
		{
			ultimo.cambiarSiguiente(nodo1);
		}

		ultimo = nodo1;
		size++;
	}

	public T dequeue() 
	{
		if ( esVacio() )
		{
			System.out.println("No hay elementos en la cola");
		}
		else
		{
			T nodo1 =  cabeza.darElemento(); 
			cabeza = cabeza.darSiguiente(); 
			size--;

			if ( esVacio() )
			{
				ultimo = null;
			}
			return nodo1; 
		}
		return null;
	}

	public T first() throws Exception
	{
		if ( esVacio() )
		{
			throw new Exception("No hay elementos en la cola");
		}
		else
		{
			return (T) cabeza.darElemento(); 
		}
	}    
	
	
	
	
	
	
	
}

